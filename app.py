from flask import Flask, render_template, request, redirect, url_for, flash
from flask_login import LoginManager, UserMixin, login_manager
import sqlite3
import os

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))
DATABASE = os.path.join(PROJECT_ROOT, 'NewsBase.db')


app = Flask(__name__)

@app.route('/news', methods = ['GET','POST'])
def index():
    if request.method == 'POST':
        posts = []
        prom_dict = {}
        conn = sqlite3.connect(DATABASE)
        c = conn.cursor()
        content_filter = '.,?!:; /\[]'
        content_search = request.form.get('content_search')
        if content_search not in content_filter and len(content_search) >= 2:
            itog = c.execute("SELECT * FROM News_info WHERE content like '%{}%'".format(content_search)).fetchall()

            if len(itog) != 0:
                for i in itog:
                    prom_dict['header'] = i[1]
                    prom_dict['content'] = i[2]
                    posts.append(prom_dict)
                    prom_dict = {}
                return render_template('News.html', posts=posts)
            else:
                posts = [
                    {
                        'header': "Ошибка",
                        'content': "Такого нет ни в одной новости"
                    }
                ]
                return render_template('News1.html', phrase = "Ошибка! Такого нет ни в одной новости.")
        else:
            return render_template('News1.html', phrase = 'Введите корректный поисковой запрос')
    posts = [
        {
            'header': "MoscowNews",
            'content': "Около 50 тысяч порций блинов съели горожане на «Московской Масленице»"
        },
        {
            'header': "RostovNews",
            'content': 'Ростов занял второе место в стране по качеству оказания образовательных услуг в школах и детских садах'
        },
        {
            'header': "SpbNews",
            'content': 'В Петербурге наблюдали редкое явление – световые столбы'
        }
    ]
    return render_template('News.html', posts = posts)
